import {Dispatch} from 'redux';
import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from './Consts';
import {ILoginData} from './Models';

/**
 * Экшены для приложения.
 */
export class Actions {
    constructor(private dispatch: Dispatch<IActionType>) {
    }

    onLogin = (loginData?: ILoginData) => {
        if(sessionStorage.getItem('login')){
            this.dispatch({
                type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`
            });
        }
        else {
            this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`});
            const options = {
                method: 'POST',
                body: JSON.stringify(loginData),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
            };
            fetch('http://127.0.0.1:8080/athorize', options)
                .then(response => {
                    console.log(response.status)
                    if (response.status === 200) {
                        return response.json();
                    } else {
                        throw 'error';
                    }
                }).then(answer => {
                if (answer.isLogin) {
                    sessionStorage.setItem('login', loginData.loginData.login);
                    this.dispatch({
                        type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`,
                        payload: loginData.loginData.login
                    });
                }
                else {
                    this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`});
                }
            })
                .catch(error => {
                    console.log(error)
                    this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error});
                });
        }
    };

    onLogout = () => {
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                loginData: {
                    login: 'logout',
                    password: 'true'
                }
            })
        };
        fetch('http://127.0.0.1:8080/athorize', options)
            .then(response => {
                if (response.status === 200) {
                    sessionStorage.clear();
                    this.dispatch({type: ActionTypes.LOGOUT});
                } else {
                    throw 'error';
                }
            })
            .catch(() => {
                this.dispatch({type: ActionTypes.LOGOUT});
            });
    };
    getAllOrganization = () => {
        const option = {
            method: 'GET'
        };
        this.dispatch({type:`${ActionTypes.ORG_LIST}${AsyncActionTypes.BEGIN}`});
         fetch('http://127.0.0.1:8080/organization', option).then(response => {
            if (response.status === 200) {
                return response.json();
            }
            else {
                throw 'error';
            }
        }).then(json => {
            console.log(json);
            this.dispatch({type:`${ActionTypes.ORG_LIST}${AsyncActionTypes.SUCCESS}`,payload:json});
        }).catch(error => {
            console.log(error);
        })
    };
    getDivisions = (id: string)=>{
        let url = new URL('http://127.0.0.1:8080/division');
        url.searchParams.append('id', id);
        const option = {
            method: 'GET'
        };
        this.dispatch({type:`${ActionTypes.ORG_LIST}${AsyncActionTypes.BEGIN}`});
        fetch(url, option).then(response => {
            if (response.status === 200) {
                return response.json();
            }
            else {
                throw 'error';
            }
        }).then(json => {
            console.log(json);
            this.dispatch({type:`${ActionTypes.DIV_LIST}${AsyncActionTypes.SUCCESS}`,payload:{data: json,id: id}});
        }).catch(error => {
            console.log(error);
        })
    };
    getEmployees = (id: string)=>{
        let url = new URL('http://127.0.0.1:8080/employee');
        url.searchParams.append('id', id);
        const option = {
            method: 'GET'
        };
        this.dispatch({type:`${ActionTypes.ORG_LIST}${AsyncActionTypes.BEGIN}`});
        fetch(url, option).then(response => {
            if (response.status === 200) {
                return response.json();
            }
            else {
                throw 'error';
            }
        }).then(json => {
            console.log(json);
            this.dispatch({type:`${ActionTypes.EMPL_LIST}${AsyncActionTypes.SUCCESS}`,payload:{data: json,id: id}});
        }).catch(error => {
            console.log(error);
        })
    };
    showModal = (modalAction: string, data?: any) =>{
        if(data){
            this.dispatch({type:`${ActionTypes.SHOW_MODAL}`, payload:{modalAction, data}});
        }
        else {
            this.dispatch({type:`${ActionTypes.SHOW_MODAL}`,payload:{modalAction}});
        }
    };
    hideModal = () =>{
        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`})
    };
    sendData = (action: string, data?: FormData) => {
        let url: string, method: string;
        let object: any = {};
        data.forEach((value, key) => {object[key] = value});
        let json = JSON.stringify(object);
        switch (action){
            case 'Add Organization':
                url='http://127.0.0.1:8080/createOrganization';
                method='POST';
                break;
            case 'Edit Organization':
                url='http://127.0.0.1:8080/editOrganization';
                method='PUT';
                break;
            case 'Delete Organization':
                url = new URL('http://127.0.0.1:8080/deleteOrganization');
                url.searchParams.append('id',object.id);
                method='DELETE';
                break;
            case 'Add Division':
                url='http://127.0.0.1:8080/createDivision';
                method='POST';
                break;
            case 'Edit Division':
                url='http://127.0.0.1:8080/editDivision';
                method='PUT';
                break;
            case 'Delete Division':
                url = new URL('http://127.0.0.1:8080/deleteDivision');
                url.searchParams.append('id',object.id);
                method='DELETE';
                break;
            case 'Add Employee':
                url='http://127.0.0.1:8080/createEmployee';
                method='POST';
                break;
            case 'Edit Employee':
                url='http://127.0.0.1:8080/editEmployee';
                method='PUT';
                break;
            case 'Delete Employee':
                url = new URL('http://127.0.0.1:8080/deleteEmployee');
                url.searchParams.append('id',object.id);
                method='DELETE';
                break;
        }
        console.log(json);
        const options = {
            method: method,
            body: json,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        };
        fetch(url, options)
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    throw 'error';
                }
            }).then(answer => {
                switch (action){
                    case 'Add Organization':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.getAllOrganization();
                        break;
                    case 'Edit Organization':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.dispatch({type:`${ActionTypes.EDIT_LIST}`,payload:answer});
                        break;
                    case 'Delete Organization':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.dispatch({type:`${ActionTypes.DELETE_LIST}`,payload:answer});
                        break;
                    case 'Add Division':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.getDivisions(object.id_organization);
                        break;
                    case 'Edit Division':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.dispatch({type:`${ActionTypes.EDIT_LIST}`,payload:answer});
                        break;
                    case 'Delete Division':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.dispatch({type:`${ActionTypes.DELETE_LIST}`,payload:answer});
                        break;
                    case 'Add Employee':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.getEmployees(object.id_division);
                        break;
                    case 'Edit Employee':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.dispatch({type:`${ActionTypes.EDIT_LIST}`,payload:answer});
                        break;
                    case 'Delete Employee':
                        this.dispatch({type:`${ActionTypes.HIDE_MODAL}`});
                        this.dispatch({type:`${ActionTypes.DELETE_LIST}`,payload:answer});
                        break;
                }
            }).catch(error => {
                console.log(error)
            });
    }
}