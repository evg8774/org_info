/**
 * Типы экшенов, используемые в приложении.
 * LOGIN - Авторизация.
 * LOGOUT - Отмена авторизации.
 * CLICK - Подсчёт чего-либо для примера.
 */
export enum ActionTypes {
    LOGIN = 'ACTION_LOGIN',
    LOGOUT = 'ACTION_LOGOUT',
    ORG_LIST = 'ACTION_ORG_LIST',
    DIV_LIST = 'ACTION_DIV_LIST',
    EMPL_LIST = 'ACTION_EMPL_LIST',
    SHOW_MODAL='SHOW_MODAL',
    HIDE_MODAL='HIDE_MODAL',
    EDIT_LIST='EDIT_LIST',
    DELETE_LIST='DELETE_LIST',
}

/**
 * Подтипы для экшенов при ассинхронной работы.
 * BEGIN - Начало ассинхронного действия.
 * SUCCESS - Действие завершилось успешно.
 * FAILURE - Действие завершилось с ошибкой.
 */
export enum AsyncActionTypes {
    BEGIN = '_BEGIN',
    SUCCESS = '_SUCCESS',
    FAILURE = '_FAILURE',
}
