import * as React from 'react';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';
import {connect} from 'react-redux';
import {IActionType} from '../common';
import {Dispatch} from 'redux';
import {IDataDelete} from '../Actions/Models';
import {ModalWindow} from './Modal';
import {Division} from './Division';
import {IDispatchProps} from '../App/App';

interface IStateProps{
    loadingDataList: boolean;
    dataList: string;
}

export interface IData{
    id: string,
    name: string,
    phone: string,
}

type TProps = IStateProps & IDispatchProps;

class Divisions extends React.Component<TProps,{}>{
    showModal= (e, data) =>{
        let actionModal: string=e.target.value;
        switch (actionModal){
            case 'Add':
                this.props.actions.showModal(`${e.target.value} Division`);
                break;
            case 'Edit':
                let dataEdit: IData={
                    ...data
                };
                this.props.actions.showModal(`${e.target.value} Division`,dataEdit);
                break;
            case 'Delete':
                let dataDelete: IDataDelete={
                    id: data
                };
                this.props.actions.showModal(`${e.target.value} Division`,dataDelete);
                break;
        }
    };
    componentDidMount(){
        const id: string=this.props.match.params.id;
        this.props.actions.getDivisions(id);
    }
    render(){
        const addModal=<div>
            <input className="btn btn-success" onClick={this.showModal} type="button" value="Add"/>
            <ModalWindow></ModalWindow>
            </div>;
        return (<div className="list">{this.props.loadingDataList?<h2>Loading ...</h2>:
        <div className="col-md-1">{addModal}</div>}<div className="col-md-11 list">
        {this.props.loadingDataList?<h2>List divisions</h2>:
        this.props.dataList?
            this.props.dataList.map(function (item: IData) {
                return <Division showModal={this.showModal} key={item.id} item={item}/>
            },this):<h2>Divisions list is empty</h2>}
        </div></div>)
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loadingDataList: state.loadingDataList,
        dataList: state.dataList,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectDiv = connect(mapStateToProps, mapDispatchToProps)(Divisions);

export {connectDiv as Divisions};
