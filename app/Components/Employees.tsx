import * as React from 'react';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';
import {connect} from 'react-redux';
import {IActionType} from '../common';
import {Dispatch} from 'redux';
import {Employee} from './Employee';
import {ModalWindow} from './Modal';
import {IDispatchProps} from '../App/App';
import {IDataDelete} from '../Actions/Models';

interface IStateProps{
    loadingDataList: boolean;
    dataList: string;
}

export interface IData{
    id: string,
    FIO: string,
    position: string,
    address: string,
}

type TProps = IStateProps & IDispatchProps;

class Employees extends React.Component<TProps,{}>{
    showModal= (e, data) =>{
        let actionModal: string=e.target.value;
        switch (actionModal){
            case 'Add':
                this.props.actions.showModal(`${e.target.value} Employee`);
                break;
            case 'Edit':
                let editData: IData={
                    ...data
                };
                this.props.actions.showModal(`${e.target.value} Employee`,editData);
                break;
            case 'Delete':
                let deleteData: IDataDelete={
                    id: data
                };
                this.props.actions.showModal(`${e.target.value} Employee`,deleteData);
                break;
        }
    };
    componentDidMount(){
        const id=this.props.match.params.id;
        this.props.actions.getEmployees(id);
    }
    render(){
        const addModal=<div>
            <input className="btn btn-success" onClick={this.showModal} type="button" value="Add"/>
            <ModalWindow></ModalWindow>
        </div>;
        return (<div className="list">{this.props.loadingDataList?<h2>Loading ...</h2>:
            <div className="col-md-1">{addModal}</div>}<div className="col-md-11 list">
            {this.props.loadingDataList?<h2>List Employees</h2>:
                this.props.dataList?
                    this.props.dataList.map(function (item: IData) {
                        return <Employee showModal={this.showModal} key={item.id} item={item}/>
                    },this):<h2>Employees list is empty</h2>}
        </div></div>)
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loadingDataList: state.loadingDataList,
        dataList: state.dataList,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectEmpl = connect(mapStateToProps, mapDispatchToProps)(Employees);

export {connectEmpl as Employees};
