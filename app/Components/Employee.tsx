import * as React from 'react';
import {IData} from './Employees';

export class Employee extends React.Component<IData, {}>{
    render(){
        const {address, id, FIO, position}=this.props.item;
        return(<div className="item card">
            <div className="card-body">
                <h5 className="card-title">{FIO}</h5>
                <p className="card-text">Position: {position}</p>
                <p className="card-text">Address: {address}</p>
                <input className="btn btn-warning" onClick={(e)=>this.props.showModal(e,this.props.item)} type="button"  value="Edit"/>
                <input className="btn btn-danger" onClick={(e)=>this.props.showModal(e, id)} type="button" value="Delete"/>
            </div>
        </div>)
    }
}/**
 * Created by gur on 15.09.19.
 */
