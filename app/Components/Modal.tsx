import * as React from 'react';
import {Modal as ModalWindow} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';
import {Dispatch} from 'redux';
import {IActionType} from '../common';
import {connect} from 'react-redux';
import {IDispatchProps} from '../App/App';

interface IStateProps{
    showModal: boolean;
    modalData: any;
    id_parent: string;
}

type TProps = IStateProps & IDispatchProps;

class Modal extends React.Component<TProps, {}>{
    hideModal = () =>{
        this.props.actions.hideModal();
    };
    sendData = () =>{
        let formData = new FormData(document.forms.modalForm);
        this.props.actions.sendData(this.action, formData);
    };
    render(){
        this.action = this.props.modalData.modalAction;
        let modalBody;
        const addOrg = <div className="modalBody">
            <form id="modalForm">
            <input type="text" name="name" placeholder="Name"/>
            <input type="text" name="INN" placeholder="INN"/>
            <input type="text" name="address" placeholder="Address"/>
            </form>
            </div>;
        const editOrg= <div className="modalBody">
            <form id="modalForm">
                <input type="text" name="name" defaultValue={this.props.modalData.data?this.props.modalData.data.name:''} placeholder="Name"/>
                <input type="text" name="INN" defaultValue={this.props.modalData.data?this.props.modalData.data.INN:''} placeholder="INN"/>
                <input type="text" name="address" defaultValue={this.props.modalData.data?this.props.modalData.data.address:''} placeholder="Address"/>
                <input type="hidden" name="id" defaultValue={this.props.modalData.data?this.props.modalData.data.id:''}/>
            </form>
        </div>;
        const deleteOrg= <div className="modalBody">
            <form id="modalForm">
                <p>Are you sure?</p>
                <input type="hidden" name="id" defaultValue={this.props.modalData.data?this.props.modalData.data.id:''}/>
            </form>
        </div>;
        const addDiv = <div className="modalBody">
            <form id="modalForm">
                <input type="text" name="name" placeholder="Name"/>
                <input type="text" name="phone" placeholder="Phone"/>
                <input type="hidden" name="id_organization" value={this.props.id_parent?this.props.id_parent:''}/>
            </form>
        </div>;
        const editDiv = <div className="modalBody">
            <form id="modalForm">
                <input type="text" name="name" defaultValue={this.props.modalData.data?this.props.modalData.data.name:''} placeholder="Name"/>
                <input type="text" name="phone" defaultValue={this.props.modalData.data?this.props.modalData.data.phone:''} placeholder="Phone"/>
                <input type="hidden" name="id_organization" value={this.props.id_parent?this.props.id_parent:''}/>
                <input type="hidden" name="id" defaultValue={this.props.modalData.data?this.props.modalData.data.id:''}/>
            </form>
        </div>;
        const deleteDiv= <div className="modalBody">
            <form id="modalForm">
                <p>Are you sure?</p>
                <input type="hidden" name="id" defaultValue={this.props.modalData.data?this.props.modalData.data.id:''}/>
            </form>
        </div>;
        const addEmp = <div className="modalBody">
            <form id="modalForm">
                <input type="text" name="FIO" placeholder="FIO"/>
                <input type="text" name="position" placeholder="Position"/>
                <input type="address" name="address" placeholder="Address"/>
                <input type="hidden" name="id_division" value={this.props.id_parent?this.props.id_parent:''}/>
            </form>
        </div>;
        const editEmp = <div className="modalBody">
            <form id="modalForm">
                <input type="text" name="FIO" defaultValue={this.props.modalData.data?this.props.modalData.data.FIO:''} placeholder="FIO"/>
                <input type="text" name="position" defaultValue={this.props.modalData.data?this.props.modalData.data.position:''} placeholder="Position"/>
                <input type="text" name="address" defaultValue={this.props.modalData.data?this.props.modalData.data.address:''} placeholder="Address"/>
                <input type="hidden" name="id_division" value={this.props.id_parent?this.props.id_parent:''}/>
                <input type="hidden" name="id" defaultValue={this.props.modalData.data?this.props.modalData.data.id:''}/>
            </form>
        </div>;
        const deleteEmp= <div className="modalBody">
            <form id="modalForm">
                <p>Are you sure?</p>
                <input type="hidden" name="id" defaultValue={this.props.modalData.data?this.props.modalData.data.id:''}/>
            </form>
        </div>;
            switch (this.action){
                case 'Add Organization':
                    modalBody=addOrg;
                    break;
                case 'Edit Organization':
                    modalBody=editOrg;
                    break;
                case 'Delete Organization':
                    modalBody=deleteOrg;
                    break;
                case 'Add Division':
                    modalBody=addDiv;
                    break;
                case 'Edit Division':
                    modalBody=editDiv;
                    break;
                case 'Delete Division':
                    modalBody=deleteDiv;
                    break;
                case 'Add Employee':
                    modalBody=addEmp;
                    break;
                case 'Edit Employee':
                    modalBody=editEmp;
                    break;
                case 'Delete Employee':
                    modalBody=deleteEmp;
                    break;
            }
        return(
            <ModalWindow
                show={this.props.showModal}
                onHide={this.hideModal}
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <ModalWindow.Header closeButton>
                    <ModalWindow.Title id="contained-modal-title-vcenter">
                        {this.props.modalData.modalAction}
                    </ModalWindow.Title>
                </ModalWindow.Header>
                <ModalWindow.Body>
                    {modalBody}
                </ModalWindow.Body>
                <ModalWindow.Footer>
                    <Button onClick={this.sendData}>{this.action?this.action.split(' ',1):''}</Button>
                    <Button onClick={this.hideModal}>Close</Button>
                </ModalWindow.Footer>
            </ModalWindow>)
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        showModal: state.showModal,
        modalData: state.modalData,
        id_parent: state.id_parent,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectModal = connect(mapStateToProps, mapDispatchToProps)(Modal);

export {connectModal as ModalWindow};