import * as React from 'react';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';
import {connect} from 'react-redux';
import {IActionType} from '../common';
import {Dispatch} from 'redux';
import {Organization} from './Organization';
import {ModalWindow} from './Modal';
import {IDispatchProps} from '../App/App';
import {IDataDelete} from '../Actions/Models';

interface IStateProps{
    loadingDataList: boolean;
    dataList: string;
}

export interface IData{
    id: string,
    name: string,
    inn: string,
    address: string,
}

type TProps = IStateProps & IDispatchProps;

class Organizations extends React.Component<TProps,{}>{
    showModal= (e, data) =>{
        let actionModal: string=e.target.value;
        switch (actionModal){
            case 'Add':
                this.props.actions.showModal(`${e.target.value} Organization`);
                break;
            case 'Edit':
                let editData: IData={
                    ...data
                };
                this.props.actions.showModal(`${e.target.value} Organization`, editData);
                break;
            case 'Delete':
                let deleteData: IDataDelete={
                    id: data
                };
                this.props.actions.showModal(`${e.target.value} Organization`,deleteData);
                break;
        }
    };
    componentDidMount(){
        this.props.actions.getAllOrganization();
    }
    render(){
        const addModal=<div>
            <input className="btn btn-success" onClick={this.showModal} type="button" value="Add"/>
            <ModalWindow></ModalWindow>
        </div>;
        return (<div className="list">{this.props.loadingDataList?<h2>Loading ...</h2>:
            <div className="col-md-1">{addModal}</div>}<div className="col-md-11 list">
            {this.props.loadingDataList?<h2>List organization</h2>:
            this.props.dataList?
                this.props.dataList.map(function (item: IData) {
                  return <Organization showModal={this.showModal} key={item.id} item={item}/>
                },this):<h2>Organizations list is empty</h2>}
        </div></div>)
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loadingDataList: state.loadingDataList,
        dataList: state.dataList,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectOrg = connect(mapStateToProps, mapDispatchToProps)(Organizations);

export {connectOrg as Organizations};
