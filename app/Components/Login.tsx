/**
 * Created by gur on 04.09.19.
 */
import * as React from 'react';
import {Actions} from '../Actions/Actions';
import {IStoreState} from '../Reducers/Reducers';
import {connect} from 'react-redux';
import {IActionType} from '../common';
import {Dispatch} from 'redux';
import {Redirect} from 'react-router';
import {IDispatchProps} from '../App/App';

interface IStateProps{
    loginStatus: boolean;
    waitingForLogin: boolean;
    currentLogin: string;
    login: string,
    password: string
}

interface ILoginData{
    loginData: {
        login: string,
        password: string
    }
}
interface ILocalState{
    login: string,
    password: string
}

type TProps = IStateProps & IDispatchProps;

 class Login extends React.Component<TProps, ILocalState>{
     constructor(props){
         super(props);
         this.state ={
                 login: "",
                 password: ""
         };
     }
     handleLogin = () => {
         let loginData: ILoginData={
             loginData:{
                 ...this.state
             }
         };
         if(loginData.name!=='' && loginData.password!==''){
             this.props.actions.onLogin(loginData);}
         else {
             alert('Insert all fields');}
     };
    render(){
        if(sessionStorage.getItem('login')){
            return(<Redirect to="/organization"/>)
        }
        const {loginStatus} = this.props;
        return(
            <div className="row">
            {loginStatus ? <Redirect to="/" />:
                <div className="col-md-4 offset-md-4 authorization">
                    <h2>Authorization</h2>
                    <div><input
                        id="loginField"
                        type="text"
                        placeholder="login"
                        onChange={(e)=>{this.setState({login: e.target.value})}}
                    /></div>
                    <div>
                        <input
                            id="passwordField"
                            type="text"
                            placeholder="password"
                            onChange={(e)=>{this.setState({password: e.target.value})}}
                        />
                    </div>
                    <div><input
                        className="btn btn-primary"
                        type="button"
                        value="LogIn"
                        onClick={this.handleLogin}
                    /></div>
                </div>
            }</div>

        )
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loginStatus: state.loginStatus,
        waitingForLogin: state.loading,
        currentLogin: state.currentLogin,
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectLogin = connect(mapStateToProps, mapDispatchToProps)(Login);

export {connectLogin as Login};

