import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {IData} from './Divisions';

export class Division extends React.Component<IData,{}>{
    render(){
        const {phone,id,name}=this.props.item;
        return(<div className="item card">
        <div className="card-body">
        <h5 className="card-title">{name}</h5>
        <p className="card-text">Phone: {phone}</p>
        <NavLink className="btn btn-primary" to={`/worker/${id}`}>Employee</NavLink>
        <input className="btn btn-warning" onClick={(e)=>this.props.showModal(e,this.props.item)} type="button" value="Edit"/>
        <input className="btn btn-danger" onClick={(e)=>this.props.showModal(e,id)} type="button" value="Delete"/>
            </div>
            </div>)
    }
}/**
 * Created by gur on 15.09.19.
 */
