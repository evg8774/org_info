import * as React from 'react';
import {NavLink} from 'react-router-dom';
import {IData} from './Organizations';

export class Organization extends React.Component<IData, {}>{
    render(){
        const {INN,address,id,name}=this.props.item;
        return(<div className="item card">
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text">INN: {INN}</p>
                <p className="card-text">Address: {address}</p>
                <NavLink className="btn btn-primary" to={`/department/${id}`}>Division</NavLink>
                <input className="btn btn-warning" onClick={(e)=>this.props.showModal(e,this.props.item)} type="button" value="Edit"/>
                <input className="btn btn-danger" onClick={(e)=>this.props.showModal(e, id)} type="button" value="Delete"/>
            </div>
        </div>)
    }
}