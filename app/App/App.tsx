import * as React from 'react';
import * as ReactRouterDOM from 'react-router-dom';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {IActionType} from '../common';
import {Actions} from '../Actions/Actions';
import {Login} from '../Components/Login';
import {Organizations} from '../Components/Organizations';
import {IStoreState} from '../Reducers/Reducers';
import {Redirect} from 'react-router';
import {Divisions} from '../Components/Divisions';
import {Employees} from '../Components/Employees';
import './App.less';

/**
 * Пропсы компонента из стора.
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} waitingForLogin Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {string} currentLogin Текущий пользователь.
 */
interface IStateProps{
    loginStatus: boolean;
    waitingForLogin: boolean;
    currentLogin: string;
}

/**
 * Пропсы для передачи экшенов.
 * @prop {Actions} actions Экшены для работы приложения.
 */
export interface IDispatchProps{
    actions: Actions;
}

/**
 * Итоговые пропсы компонента
 */
type TProps = IStateProps & IDispatchProps;

/**
 * Компоненты роутинга
 */
const Router = ReactRouterDOM.BrowserRouter;
const Route = ReactRouterDOM.Route;
const Switch = ReactRouterDOM.Switch;

/**
 * Основной класс приложения.
 */
class App extends React.Component<TProps, {}> {
    /**
     * Обработчик выхода из системы.
     */
    handleLogout = () => this.props.actions.onLogout();//выход из приложения

    render() {
        let currentLogin: string = sessionStorage.getItem('login');
        if(currentLogin){
            this.props.actions.onLogin();
        }
        return (
            <div className="container-fluid">
                <div className="row titleApp">
                    <h2>
                        Organization Info
                    </h2>
                    {currentLogin ? <div><label className="badge badge-info">{currentLogin}</label><input id="logOut" onClick={this.handleLogout} className="btn btn-danger" type="button" value="LogOut"/></div>:''}
                </div>
            <Router>
                <Switch>
                    <Route  path="/login" component={Login}/>
                    {!currentLogin?<Redirect to="/login"/>:''}
                    <Route  path="/organization" component={Organizations}/>
                    <Route  path="/department/:id" component={Divisions}/>
                    <Route  path="/worker/:id" component={Employees}/>
                    <Redirect from="/" to="/organization"/>
                </Switch>
            </Router>
            </div>

        );
    }
}

function mapStateToProps(state: IStoreState): IStateProps {
    return {
        loginStatus: state.loginStatus
    };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatchProps {
    return {
        actions: new Actions(dispatch)
    };
}

const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export {connectApp as App};
