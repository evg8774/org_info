import {IActionType} from '../common';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';

/**
 * Состояние для Redux хранилища (стора).
 * @prop {boolean} loginStatus Состояние зарегистрированности пользователя.
 * @prop {boolean} loading Ожидание завершения процедуры авторизации (завершение логина).
 * @prop {boolean} counter Результат вычисления.
 * @prop {boolean} counterIsLoading Выполнение вычисления.
 */
export interface IStoreState{
    loginStatus: boolean;
    loading: boolean;
    loadingDataList: boolean;
    dataList: string;
    currentLogin: string,
    showModal: boolean,
    modalData: any,
    id_parent: string,
    login: string,
    password: string
}

/**
 * Начальное состояние стора.
 */
const initialState = {
    get state(): IStoreState {
        return {
            loginStatus: false,
            loading: false,
            loadingDataList: false,
            dataList: '',
            currentLogin:'',
            showModal: false,
            modalData: '',
            id_parent: '',
        };
    }
};

export function reducer(state: IStoreState = initialState.state, action: IActionType) {
    switch (action.type) {
        case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
            return {
                ...state,
                loading: true,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
            return {
                ...state,
                loginStatus: true,
                loading: false,
                currentLogin:action.payload,
            };

        case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
            return {
                ...state,
                loading: false,
                loginStatus: false,
            };

        case ActionTypes.LOGOUT:
            return {
                ...state,
                loginStatus: false,
            };
        case `${ActionTypes.ORG_LIST}${AsyncActionTypes.SUCCESS}`:
            return{
                ...state,
                loadingDataList:false,
                dataList:action.payload
            };
        case `${ActionTypes.ORG_LIST}${AsyncActionTypes.BEGIN}`:
            return{
                ...state,
                loadingDataList:true,
            };
        case `${ActionTypes.SHOW_MODAL}`:
            return{
                ...state,
                showModal:true,
                modalData: action.payload,
            };
        case `${ActionTypes.HIDE_MODAL}`:
            return{
                ...state,
                showModal:false,
            };
        case `${ActionTypes.EDIT_LIST}`:
            let newListEdit=state.dataList.map(function (item: any) {
                if(item.id===action.payload.id){
                    return action.payload;
                }
                else return item;
            });
            return{
                ...state,
                dataList: newListEdit
            };
        case `${ActionTypes.DELETE_LIST}`:
            let newListDel = [...state.dataList];
            state.dataList.forEach(function (item: any,index: number) {
                if(item.id===action.payload.id){
                    newListDel.splice(index,1);
                }
            });
            return{
                ...state,
                dataList: newListDel
            };
        case `${ActionTypes.DIV_LIST}${AsyncActionTypes.SUCCESS}`:
            return{
                ...state,
                loadingDataList:false,
                dataList:action.payload.data,
                id_parent:action.payload.id
            };
        case `${ActionTypes.EMPL_LIST}${AsyncActionTypes.SUCCESS}`:
            return{
                ...state,
                loadingDataList:false,
                dataList:action.payload.data,
                id_parent:action.payload.id
            };
    }
    return state;
}
